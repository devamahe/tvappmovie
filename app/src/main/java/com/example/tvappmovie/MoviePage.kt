package com.example.tvappmovie

import com.google.gson.annotations.SerializedName

data class MoviePage(
    @SerializedName("results")
    val result: List<Movie>? = null)


