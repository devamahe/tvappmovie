package com.example.tvappmovie

//import androidx.lifecycle.Observer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class TvViewModel : ViewModel() {

    val repository = Repository(ApiFactory.tmdbApi)

    val moviesLiveData : LiveData<MoviePage> = repository.getPopularMovies()


    fun getPopularMovies(): MutableLiveData<MoviePage> {
      return  repository.getPopularMovies()
    }
}














