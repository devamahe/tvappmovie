package com.example.tvappmovie

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(private  val service: API) {
    val moviesLiveData = MutableLiveData<MoviePage>()

    fun getPopularMovies(): MutableLiveData<MoviePage> {
        Log.e("LOG", "s")
        service.getPopularMovies().enqueue(object : Callback<MoviePage>{
            override fun onFailure(call: Call<MoviePage>, t: Throwable) {
                Log.e(TAG, "Failed getting MovieDetails: " )           }

            override fun onResponse(call: Call<MoviePage>, response: Response<MoviePage>) {
                if (response.isSuccessful) {
                    val moviePage: MoviePage? = response.body()
                    moviesLiveData.value = moviePage
                    Log.e(TAG, "check : " + response.body()?.result)
                }
            }

        })
        return moviesLiveData
    }

}