package com.example.tvappmovie

import com.google.gson.annotations.SerializedName

class MovieResponse {
    var page: Long = 0
    @SerializedName("total_pages")
    var totalPages: Long = 0
    @SerializedName("total_results")
    var totalResults: Long = 0
    private var results: List<Movie>

    fun getResults(): List<Movie> {
        return results
    }

    fun setResults(results: List<Movie>) {
        this.results = results
    }

    init {
        results = ArrayList()
    }
}